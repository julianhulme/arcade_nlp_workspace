# loop through every folder in the messages directory
import json
import os
from os.path import isdir

def extract_message_data_from_file(json_file):
    # get the json data out of the file

    my_file = open(json_file, "r")

    json_str = my_file.read()

    json_data = json.loads(json_str)

    # uncomment the line below to print the json data for each file
    # print(json_data)

    other_username = ""
    for participant in json_data["participants"]:
        if participant["name"] != host_name:
            other_username = participant["name"]

    messages = json_data["messages"]
    for message in messages:
        if "content" not in message:
            continue # skip this message and continue the for loop as it has no content. Some messages are photos, which dont have text

        content = message["content"]
        content = content.replace("\n", ". ") # replace new lines with just start of new sentences. new lines skrew up the csv export

        sender = message["sender_name"]
        if sender == host_name:
            receiver = other_username
        else:
            receiver = host_name
        message_text = content
        timestamp = message["timestamp_ms"]
        write_file = open("./messages.csv", "a")
        write_file.write("%s, %s, %s, %s\n"%(sender, receiver, message_text, timestamp))
        write_file.close()



# this is the start of the script, the code above is a function that gets called later
print("Started the message data extraction and saving it to 'scripts/messages.csv ...")
total_num_messages = 0
host_name = "PhotoArcade"


write_file = open("./messages.csv", "w")
write_file.write("sender, receiver, message, timestamp\n")
write_file.close()

# start by looping through the files in the directories
directory = r'../messages'
for filename in os.listdir(directory):
    dir_path = "../messages/%s" % filename
    if isdir(dir_path):

        for filename_in_sub_dir in os.listdir(dir_path):

            if filename_in_sub_dir == "message_1.json":
                # we have the json file that gives us data like messages etc. awesome.
                full_path_of_file = "%s/%s" % (dir_path, filename_in_sub_dir)

                # look at the file and write the message data to csv
                extract_message_data_from_file(full_path_of_file)


print("done. Look at the function 'extract_message_data_from_file' to update the data that is being written. ")

