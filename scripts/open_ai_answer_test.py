# Import the openai framework
import openai

# specify the ARCADE api key
openai.api_key = "sk-40G6yuc9VAUxEHH1cQuOCevpfiseNlGEGfX96TjP"

# create_response = openai.File.create(file=open("scripts/arcade_answers.jsonl"), purpose='answers')
# file_id = create_response.id

# provide the question to send to the API
question = "which puppy is the least happy"

# a local list of answer documents that can be added to the API query
doc_list = ['Puppy A is happy', 'Puppy B is sad.', 'Puppy C is the happiest']

# Make the API call to OpenAI
response = openai.Answer.create(
    search_model="ada",
    model="curie",
    question=question,
    # file=file_id, # You can comment out this line and uncomment the line below to use a local list of answers instead of the file
    documents=doc_list,
    examples_context="In 2017, U.S. life expectancy was 78.6 years.",
    examples=[["What is human life expectancy in the United States?", "78 years."]],
    max_rerank=10,
    max_tokens=5,
    stop=["\n", "<|endoftext|>"]
)

# get the response text(s) from the api call
answers = response.answers
if len(answers) == 0:
  print ("no answer returned")
elif len(answers) == 1:
  print("answer: %s" % answers[0])
else:
  print("multiple answers returned, see below:")
  for answer in answers:
    print(answer)
