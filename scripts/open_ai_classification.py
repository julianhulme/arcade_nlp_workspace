# Import the openai framework
import openai
import os

# specify the ARCADE api key
openai.api_key = "sk-40G6yuc9VAUxEHH1cQuOCevpfiseNlGEGfX96TjP"

#reate_response = openai.File.create(file=open("arcade_classification.jsonl"), purpose='classifications')
#file_id = "file-pIK2mMRoeEZbvuHcjKLYLH4q" # create_response.id

# see the list below
example_list=[
    ["I forgot my password", "Use Case 1"],
    ["How do I take part in the contest?", "Use Case 2"],
    ["How do I get into the site?", "Use Case 3"],
    ["How long does the contest take?", "Use Case 4"],
    ["What to do if I forgot my password?", "Use Case 1"],
    ["What do I do if I forgot my password?", "Use Case 1"],
    ["I don't remember my password.", "Use Case 1"],
    ["How do I change my password?", "Use Case 1"],
    ["How do I join the contest?", "Use Case 2"],
    ["How do I enter the contest?", "Use Case 2"],
    ["What to do in order to join the contest?", "Use Case 2"],
    ["How should I enter the site?", "Use Case 3"],
    ["How to find your website?", "Use Case 3"],
    ["How do I log into the website?", "Use Case 3"],
    ["How long does the contest go on for?", "Use Case 4"],
    ["When does the contest start?", "Use Case 4"],
    ["When does the contest end?", "Use Case 4"],
]

# Make the API call to OpenAI
response = openai.Classification.create(
    #file="file-pIK2mMRoeEZbvuHcjKLYLH4q",
    examples=example_list,
    query="When was Einsteins birthday?",
    labels = ["Use Case 1", "Use Case 2", "Use Case 3", "Use Case 4"],
    search_model="davinci",
    model="davinci",
    max_examples=3
)

# get the response text(s) from the api call
answer = response.label
print(answer)

# switch between the possible paths
if answer == "Use case 1":
  print ("Please reset you password and login with this link: photo.arcade.ai")
elif answer == "Use case 2":
  print("You can join a contest by uploading a photo to your Instagram account and adding our "
        "contest hashtag  #GoldenHourArcade to your caption. You can always see our bio link for details "
        "and if you’ve entered correctly!")
elif answer == "Use case 3":
  print("See our bio link to enter")
elif answer == "Use case 4":
  print("The contest started on Monday, 19th of July and ends on Saturday 31st of July")
else:
  print("Error. Try Again")
