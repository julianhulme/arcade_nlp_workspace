import openai

# function that calls OpenAI and executes query a completion request
# the query_text input is a string. It is the query to be passed to OpenAI
# the function returns the completion that OpenAI provided in its response
def query_openai(query_text, human_escalate_text, context_data ):
    openai.api_key = "sk-40G6yuc9VAUxEHH1cQuOCevpfiseNlGEGfX96TjP"

    # specify the question you have in this string
    context = "This is a conversation between an Instagram user and a smart virtual assistant. The virtual assistant can answer only questions related to the contest. " \
              "I am an answering bot that does not know anything except the data below. " \
              "If you ask me a question that is related to the data below, I will give you the answer. " \
              "If you ask me a question that is not related to the data below, I will not respond\n\n" \
              "Q: How do I take part in the contest?\n" \
              "A: You can join a contest by uploading a photo to your Instagram account and adding our contest hashtag #GoldenHourArcade to your caption. You can always see our bio link for details and if you’ve entered correctly!" \
              "Q: How long does the contest take?\n" \
              "A: The contest started on Monday, 19th of July and ends on Saturday 31st of July\n\n" \
              "Q: How do I get into the website?\n" \
              "A: See our Instagram bio link to enter\n\n" \
              "Q: How do I go to my photo.arcade account?\n" \
              "A: See our Instagram bio link to enter\n\n" \
              "Q: What do I do if I forgot my password?\n" \
              "A: Please reset you password and login with this link: photo.arcade.ai\n\n" \
              "Q: What is the meaning of life?\n" \
              "A: I am unauthorized to answer this question.\n\n" \
              "Q: What should I do today?\n" \
              "A: I am unauthorized to answer this question.\n\n" \
              "Q: What color is the sky??\n" \
              "A: I am unauthorized to answer this question.\n\n" \
              "Q: What is the capital of Brazil??\n" \
              "A: I am unauthorized to answer this question.\n\n" \
              "Q: How many letters are in the alphabet??\n" \
              "A: I am unauthorized to answer this question.\n\n"

    query = "%s\nQ:%s\nA:" % (context, query_text)
    response = openai.Completion.create(
        engine="davinci",
        prompt=query,
        temperature=0,
        max_tokens=100,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0,
        stop=["\n"]
    )
    start_sequence = "\nA:"
    restart_sequence = "\n\nQ: "
    choices = response.choices
    if len(choices) == 0:
        print("no answer returned")
    elif len(choices) == 1:
        completion_text = choices[0].text
    else:
        print("multiple answers returned, see below:")
        completion_text = ""
        for choice in choices:
            completion_text += choice.text
    return completion_text