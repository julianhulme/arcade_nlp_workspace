# Import the openai framework
import openai

# specify the ARCADE api key
openai.api_key = "sk-40G6yuc9VAUxEHH1cQuOCevpfiseNlGEGfX96TjP"

# specify the question you have in this string
context = "I am a highly intelligent question answering bot. " \
           "If you ask me a question that is rooted in truth, I will give you the answer. " \
           "If you ask me a question that is nonsense, trickery, or has no clear answer, I will respond with \"Let me check with my human and get back to you.\".\n\n" \
          "The American president is Joe Biden\n\n"\
          "Q: How do I take part in the contest?\n" \
           "A: You can join a contest by uploading a photo to your Instagram account and adding our contest hashtag  #GoldenHourArcade to your caption. You can always see our bio link for details and if you’ve entered correctly!"\
           "Q: How long does the contest take?\n"\
           "A: The contest started on Monday, 19th of July and ends on Saturday 31st of July\n\n"\
           "Q: How do I get into the site?\n"\
           "A: see our bio link to enter\n\n"\

question = "When do we start?"


query = "%s\nQ:%s\nA:" % (context, question)

print("question: %s" % question)


# Make the API call to OpenAI
response = openai.Completion.create(
  engine="davinci",
  prompt=query,
  temperature=0,
  max_tokens=100,
  top_p=1,
  frequency_penalty=0,
  presence_penalty=0,
  stop=["\n"]
)

start_sequence = "\nA:"
restart_sequence = "\n\nQ: "


# get the response text(s) from the api call
choices = response.choices
if len(choices) == 0:
  print ("no answer returned")
elif len(choices) == 1:
  print("answer: %s" % choices[0].text)
else:
  print("multiple answers returned, see below:")
  for choice in choices:
    print(choice.text)
