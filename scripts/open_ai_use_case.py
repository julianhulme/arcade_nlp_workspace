# Import the openai framework
import openai

# specify the ARCADE api key
openai.api_key = "sk-40G6yuc9VAUxEHH1cQuOCevpfiseNlGEGfX96TjP"

#create_response = openai.File.create(file=open("arcade_answers_try.jsonl"), purpose='answers')
#file_id = "file-SGGMxyu3Uu6ow2VY7VFsY88J" # create_response.id

# a local list of answer documents that can be added to the API query
document_list = ["In the Instagram database, if somebody forgets their password, they have to reset their "
                 "password and login with this link: photo.arcade.ai.",
"You can join a contest by uploading a photo to your Instagram account and adding our contest hashtag  "
                "#GoldenHourArcade to your caption.",
"See the link in our Instagram bio in order to enter our site!",
"In order to get into our site, you have to find the link in our Instagram bio!",
"The contest started on Monday, 19th of July and ends on Saturday 31st of July."]

# Make the API call to OpenAI
response = openai.Answer.create(
    search_model="davinci",
    model="davinci",
    question="When was Einsteins birthday?",
    #file=file_id, # You can comment out this line and uncomment the line below to use a local list of answers instead of the file
    documents=document_list,
    examples_context="In 2017, U.S. life expectancy was 78.6 years.",
    examples=[["What is human life expectancy in the United States?", "Human life expectancy in the United States "
                                                                      "was 78 years."]],
    max_rerank=1,
    max_tokens=100,
    stop=["\n", "<|endoftext|>"]
)

# get the response text(s) from the api call
answers = response.answers
if len(answers) == 0:
  print ("no answer returned")
elif len(answers) == 1:
  print(answers[0])
else:
  print("multiple answers returned, see below:")
  for answer in answers:
    print(answer)
