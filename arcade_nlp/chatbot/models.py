from django.db import models
import openai

# Create your models here.
import chatbot


VERDICT_RESPONSE_CHOICE = (
    ("satisfactory", "Satisfactory"),
    ("unsatisfactory", "Unsatisfactory"),
)

class Completion(models.Model):
    query = models.CharField(max_length=5012, default="")
    response = models.CharField(max_length=5012, default="")
    context = models.CharField(max_length=5012, default="")
    question = models.CharField(max_length=5012, default="")

    has_been_executed = models.BooleanField(default=False)

    response_verdict = models.CharField(choices=VERDICT_RESPONSE_CHOICE, max_length=28)
    response_verdict_notes = models.CharField(max_length=512, default="")

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def execute_query(self):

        if not self.has_been_executed:
            completion_text = chatbot.utils.query_openai(self.query)
            self.response = completion_text
            self.save()

