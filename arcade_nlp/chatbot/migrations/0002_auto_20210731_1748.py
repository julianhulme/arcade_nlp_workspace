# Generated by Django 3.2.5 on 2021-07-31 17:48

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('chatbot', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='completion',
            name='context',
            field=models.CharField(default='', max_length=5012),
        ),
        migrations.AddField(
            model_name='completion',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='completion',
            name='has_been_executed',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='completion',
            name='modified_at',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='completion',
            name='question',
            field=models.CharField(default='', max_length=5012),
        ),
    ]
