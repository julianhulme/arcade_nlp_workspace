from django.contrib import admin

# Register your models here.
from chatbot.models import Completion


class CompletionAdmin(admin.ModelAdmin):
    list_display = ["query", "response"]

    def execute_query(self, obj):
        pass

    def execute_queries(modeladmin, request, queryset):

        for obj in queryset:
            obj.execute_query()

    actions = [execute_queries]

admin.site.register(Completion, CompletionAdmin)
