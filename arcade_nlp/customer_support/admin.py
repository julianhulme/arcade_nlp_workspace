from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import User, Group

from customer_support.models import Enquiry
admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(Enquiry)