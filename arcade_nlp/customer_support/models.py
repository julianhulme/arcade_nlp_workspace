from django.db import models

# Create your models here.


class Enquiry(models.Model):
    trained_question = models.TextField(max_length=5012, default="")
    trained_answer = models.TextField(max_length=5012, default="")

    class Meta:
        verbose_name_plural = "Enquiries"
